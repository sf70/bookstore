# Запуск проекта
#### With webpack
```bash
yarn install && yarn run serve
```

#### With docker
```bash
docker build -t books:1.0.0 .
docker run -it -d --rm -p 8080:8080 --name book-store books:1.0.0
```

далее перейти на http://127.0.0.1:8080/

для остановки контейнера
```bash
docker kill book-store
```