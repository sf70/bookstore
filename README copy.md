# Запуск проекта
#### Установка зависимостей
```bash
yarn 
```

#### Инициализация проекта
```bash
yarn init
```

# TypeScript
```bash
yarn global add typescript || sudo npm install -g typescript
tsc -v
tsc --init
```

# SCSS
```bash
yarn global add node-sass || sudo npm install -g node-sass
```

#### Сборка CSS файлов вручную
```bash
npx node-sass ./src/sass/ -o ./dist/css/
```

# 1.Подключение webpack

```bash
yarn add -D webpack webpack-cli || npm install webpack webpack-cli --save-dev
```

# Собрать файлы
```bash
npx webpack 
npx webpack --watch
npx webpack --mode production # соберет файл main.js  в ону строку
```

# Конфигурация
```bash
cat <<EOF >webpack.config.js
module.exports = {
    entry: './src/index.js',
    output: {
      filename: 'main.js'
    }
    mode: 'development'
}
EOF
```

# Linter

```bash
yarn add global -D eslint tslint || npm install eslint tslint --save-dev # Линтер
npx eslint --init
npx tslint --init
```
После этого, чтобы избавиться от еще одного конфигурационного файла в корне проекта поместить содержимой `.eslintrc.*` в `package.json` следующим образом: `"eslintConfig": ПОЛНОСТЬЮ_СОДЕРЖИМОЕ_ФАЙЛА_ESLINTRC_CONFIG`
```bash
rm -rf .eslintrc.*
```
```bash
yarn add -D stylelint-config-recommended || npm i stylelint-config-recommended --save-dev
```
Далее добавить в `package.json`:
```
"stylelint": {
  "extends": "stylelint-config-recommended"
}
```

# MockServer
```bash
yarn add global json-server
```

# Webpack + CSS + SCSS + PUG
```bash
yarn add -D css-loader style-loader                                         || npm i style-loader css-loader --save-dev                                         # загрузчик для css файлов, позволит генерить один css файл с помощью webpack
yarn add -D mini-css-extract-plugin                                         || npm install mini-css-extract-plugin --save-dev                                   # минимизатор для css файлов
yarn add -D sass-loader                                                     || npm install sass-loader --save-dev                                               # загрузчик для scss файлов, позволит генерить один css файл с помощью webpack из нескольких scss
yarn add -D css-minimizer-webpack-plugin                                    || npm install css-minimizer-webpack-plugin --save-dev                              # 
yarn add -D html-webpack-plugin                                             || npm install html-webpack-plugin --save-dev                                       # загрузчик для html файлов
yarn add -D pug pug-loader                                                  || npm install pug pug-loader --save-dev                                            # шаблонизатор
yarn add -D eslint eslint-webpack-plugin                                    || npm install eslint eslint-webpack-plugin --save-dev                              # линтер для js файлов
yarn add -D stylelint stylelint-webpack-plugin stylelint-config-recommended || npm i stylelint stylelint-webpack-plugin stylelint-config-recommended --save-dev # линтер для css файлов
yarn add -D typescript ts-loader                                            || npm install typescript ts-loader --save-dev                                      # webpack для ts файлов
yarn add -D webpack-dev-server                                              || npm install webpack-dev-server --save-dev                                        # web-сервер для запуска при разработке
yarn add -D file-loader                                                     || npm install file-loader --save-dev                                               # подгрузка файлов, картинок и т.д
yarn add -D url-loader 

cat <<EOF >webpack.config.js
const path = require('path');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const CssMinimizerPlugin = require('css-minimizer-webpack-plugin');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const ESLintPlugin = require('eslint-webpack-plugin');
const StylelintPlugin = require('stylelint-webpack-plugin');

module.exports = {
  mode: 'development',
  // entry: path.resolve(__dirname, 'src/js/index.js'),
  entry: path.resolve(__dirname, 'src/ts/index.ts'),
  output: {
    filename: 'main.js',
  },
  resolve: {
    extensions: ['.tsx', '.ts', '.js'],
  },
  devServer: {
    static: {
      directory: path.join(__dirname, 'dist'),
    },
    port: 3000,
  },
  plugins: [
    new MiniCssExtractPlugin({
      filename: 'main.css',
    }),
    new HtmlWebpackPlugin({
      template: path.resolve(__dirname, 'src/index.pug'),
      filename: 'index.html',
    }),
    new ESLintPlugin({
      extensions: ['ts'],
      exclude: ['node_modules', 'dist'],
      fix: true,
    }),
    new StylelintPlugin({
      fix: true,
      extensions: ['scss'],
      exclude: ['node_modules', 'dist'],
    }),
  ],
  module: {
    rules: [
      {
        test: /\.tsx?$/,
        use: 'ts-loader',
        exclude: /node_modules/,
      },
      {
        test: /\.scss$/i,
        use: [MiniCssExtractPlugin.loader, 'css-loader', 'sass-loader'],
      },
      {
        test: /\.pug$/i,
        use: 'pug-loader',
      },
    ],
  },
  optimization: {
    minimizer: [
      '...',
      new CssMinimizerPlugin(),
    ],
  },
};

EOF
```

# Build
## Docker

```bash
docker build -t baseapp:1.0.0 .
docker run -it -d --rm  -p 8080:8080 --name baseapp baseapp:1.0.0
```