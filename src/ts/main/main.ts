// import './slider.ts';
// import './sidebar.ts';
// import './books.ts';

import { initSidebar } from './sidebar';
import { initSlider, sliderMove } from './slider';
import { initBooks } from './books';
import { initButton } from './button';
import { initCart, setCartBadge } from './cart';

document.addEventListener("DOMContentLoaded", function() {
  setCartBadge();
  initCart();
  initSlider();
  initSidebar();
  setInterval(sliderMove, 5000);
  initBooks();
  initButton();
});