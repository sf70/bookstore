import BookLoader from '../api/api';
import {bookListRender} from './books';

const menuItems = [
  {category: "Architecture", query: ""},
  {category: "Art & Fashion", query: ""},
  {category: "Biography", query: ""},
  {category: "Business", query: ""},
  {category: "Crafts & Hobbies", query: ""},
  {category: "Drama", query: ""},
  {category: "Fiction", query: ""},
  {category: "Food & Drink", query: ""},
  {category: "Health & Wellbeing", query: ""},
  {category: "History & Politics", query: ""},
  {category: "Humor", query: ""},
  {category: "Poetry", query: ""},
  {category: "Psychology", query: ""},
  {category: "Science", query: ""},
  {category: "Technology", query: ""},
  {category: "Travel & Maps", query: ""},
];

export function initSidebar() {
  const menu = document.querySelector(".main_books__menu");
  if (menu) {
    const list: HTMLUListElement = document.createElement("ul");
    list.classList.add('books__menu__list');
    menu.appendChild(list);

    menuItems.forEach((item, index) => {
      const point: HTMLLIElement = document.createElement("li");
      point.dataset.query = item.category;
      point.id = item.category
      point.textContent = item.category
      point.classList.add('category')
      index === 0 ? point.classList.add('active') : null;
      list.appendChild(point); 
      point.addEventListener("click",() => categorySelect(item.category));
    });
  }
}

function categorySelect(category: string) {
  const menu: HTMLUListElement | null = document.querySelector(".books__menu__list");
  if (menu) {
    const currentActive = menu.querySelector(".active");
    if (currentActive) {
      currentActive.classList.remove("active")
    }
    const nextActive = document.getElementById(category)
    if (nextActive) {
      nextActive.classList.add("active");
    }
  }

  // let initialCategory: string
  // const categories: HTMLUListElement | null = document.querySelector(".books__menu__list");
  
  const bookList: HTMLElement = <HTMLElement>document.querySelector(".main_books__goods");
  bookList.innerHTML = '';
  // if (categories) {
    // const currentActive: HTMLLIElement | null = categories.querySelector(".active");
    // if (currentActive) {
      // initialCategory = currentActive.dataset.query ? currentActive.dataset.query : ""
  const loader = new BookLoader(category, 0, 6);
  loader.setParams(category, 0, 6);
  loader.getBooks().then((data) => {
    bookListRender(data, bookList)
  });
    // }
}
