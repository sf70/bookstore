export function initCart() {
  const localComments = window.localStorage.getItem('bookIDs');
  if (!localComments) {
    const c: string[] = [];
    window.localStorage.setItem('bookIDs', JSON.stringify(c));
  }
}
export function addToCart(id: string) {
  const ids: Array<string> = [];
  const cart: HTMLElement | null = document.getElementById(id);
  if (cart) {
    const button: HTMLElement | null =cart.querySelector(".cart_button")
    // let ids: string[] = [];
    const localComments = window.localStorage.getItem('bookIDs');
    if (localComments) {  
      const bla = JSON.parse(localComments);
      bla.forEach((i: string) => {ids.push(i)});
    } else {
      const c: string[] = [];
      window.localStorage.setItem('bookIDs', JSON.stringify(c));
      setCartBadge();
    }
    if (ids.includes(id)) {
      for( let i = 0; i < ids.length; i++){ 
        if ( ids[i] === id) { 
          ids.splice(i, 1); 
          if (button) {
            button.classList.remove("card__incart__button");
            button.classList.add("card__info__button");
            button.textContent = "buy now"
          }
  
        }
      }
      window.localStorage.setItem('bookIDs', JSON.stringify(ids));
      setCartBadge();
      return;
    }
    ids.push(id);
    window.localStorage.setItem('bookIDs', JSON.stringify(ids));
    setCartBadge();
    if (button) {
      button.classList.remove("card__info__button");
      button.classList.add("card__incart__button");
      button.textContent = "in the cart"
    }
  }
}

export function inCart(id: string): boolean {
  let ids: string[] = [];
  const localComments = window.localStorage.getItem('bookIDs');
  if (localComments) {  
    ids = JSON.parse(localComments);
    return ids.includes(id);
  } else {
    return false
  }
}

export function setCartBadge() {
  let ids: string[] = [];
  const cover: HTMLElement | null =document.querySelector(".bag__cover");
  const localComments = window.localStorage.getItem('bookIDs');
  if (cover) {
    const badge: HTMLElement | null = cover.querySelector(".bag__count");
    if (badge) {
      cover.removeChild(badge);
    }
    if (localComments) {
      ids = JSON.parse(localComments);
      if (ids.length !== 0) {
        const bad: HTMLSpanElement = document.createElement("span");
        bad.textContent = ids.length.toString();
        bad.classList.add("bag__count");
        cover.appendChild(bad);
      }
    }
  }
}