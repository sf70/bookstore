import BookLoader from '../api/api';
import { addToCart, inCart } from './cart';

type BookType = {
  id: string;
  volumeInfo: {
    title: string;
    authors?: Array<string>;
    description?: string;
    imageLinks: {
      smallThumbnail: string;
      thumbnail: string;
    };
    averageRating?: number;
    ratingsCount?: number;
  };
  saleInfo: {
    saleability: string;
    listPrice?: {
      amount: number;
      currencyCode: string;
    };
  };
};
class Book {
  // Класс карточки книги

  id: string;
  authors: string;
  title: string;
  description: string;
  imgSrc: string;
  rating: number | undefined;
  ratingsCount: number | undefined;
  saleability: boolean;
  price?: number;
  currency?: string;
  isInCart = false;
  // cart: Cart;

  constructor(
    id: string,
    title: string,
    saleability: string,
    imgSrc: string,
    description: string | undefined,
    authors: Array<string> | undefined,
    rating: number | undefined,
    ratingsCount: number | undefined,
    // cart: Cart,
    price: number | undefined,
    currency: string | undefined
  ) {
    this.id = id;
    if (authors) {
      this.authors = authors.join(", ");
    } else {
      this.authors = "";
    }

    this.title = title;
    this.description = description ? description : "";
    this.imgSrc = imgSrc;
    this.saleability = saleability === "FOR_SALE" ? true : false;
    if (saleability && price && currency) {
      this.price = price;
      this.currency = currency;
    }
    this.rating = rating;
    this.ratingsCount = ratingsCount;
    // this.cart = cart;
    // this.isInCart = cart.isInCart(this.id);
  }
}
type ApiItems = {
  items: Array<BookType>;
};

export function initBooks() {
  let initialCategory: string
  const categories: HTMLUListElement | null = document.querySelector(".books__menu__list");
  
  const bookList: HTMLElement = <HTMLElement>document.querySelector(".main_books__goods");
  if (categories) {
    const currentActive: HTMLLIElement | null = categories.querySelector(".active");
    if (currentActive) {
      initialCategory = currentActive.dataset.query ? currentActive.dataset.query : ""
      const loader = new BookLoader(initialCategory, 0, 6);
      loader.setParams(initialCategory, 0, 6);
      loader.getBooks().then((data) => {
        bookListRender(data, bookList)
      });
    }
  }
}

export function bookListRender(data: ApiItems, bookList: HTMLElement) {
  const items: Array<BookType> | null = data["items"];
  if (items && items.length) {
    const books: Array<Book> = [];
    for (const item of items) {
      const thumbnail: string = item.volumeInfo.imageLinks
        ? item.volumeInfo.imageLinks.thumbnail
        : "https://picsum.photos/220/340";

      books.push(
        new Book(
          item.id,
          item.volumeInfo.title,
          item.saleInfo.saleability,
          thumbnail,
          item.volumeInfo.description,
          item.volumeInfo.authors,
          item.volumeInfo.averageRating,
          item.volumeInfo.ratingsCount,
          // cart,
          item.saleInfo.listPrice?.amount,
          item.saleInfo.listPrice?.currencyCode,
        )
      );
    }

    for (const book of books) {
      const bk: HTMLDivElement = document.createElement("div");
      bk.dataset.bla = book.authors;
      bk.classList.add('book_card');
      bk.id = book.id;
      //Picture add
      const pic: HTMLImageElement = document.createElement("img");
      pic.classList.add('book_card__picture');
      pic.src = book.imgSrc;
      bk.appendChild(pic);
      //Info add
      const cartInfo: HTMLDivElement = document.createElement("div");
      cartInfo.classList.add('book_card__info');
      //Author
      const authors: HTMLDivElement = document.createElement("div");
      authors.classList.add('card__info__authors');
      authors.textContent = book.authors
      cartInfo.appendChild(authors);
      //Title
      const title: HTMLDivElement = document.createElement("div");
      title.classList.add('card__info__title');
      title.textContent = book.title;
      cartInfo.appendChild(title);
      //Rating
      const rating: HTMLDivElement = document.createElement("div");
      rating.classList.add('card__info__rating');
      //Stars ratung
      const stars: HTMLDivElement = document.createElement("div");
      stars.classList.add('card__info__stars');
      const rait = book.rating ? book.rating : 0;
      stars.style.cssText = `--rating: ${rait}`
      rating.appendChild(stars);
      //Count rating 
      const ratingsCount: HTMLDivElement = document.createElement("div");
      const views = book.ratingsCount ? book.ratingsCount.toString() : "0"
      ratingsCount.textContent = `${views} review`;
      rating.appendChild(ratingsCount);
      cartInfo.appendChild(rating);
      //Description
      const description: HTMLDivElement = document.createElement("div");
      description.textContent = book.description;
      description.classList.add('card__info__description');
      cartInfo.appendChild(description);
      //Price
      const price: HTMLDivElement = document.createElement("div");
      price.classList.add('card__info__price');
      const pr = book.price ? `$${book.price.toString()}` : "NOT FOR SALE";
      price.textContent = pr;
      cartInfo.appendChild(price);
      //Add to cart button
      if (book.price) {
        const button: HTMLButtonElement = document.createElement("button");
        button.classList.add('cart_button');
        if (inCart(book.id)) {
          button.classList.add('card__incart__button');
          button.textContent = "in the cart"
        } else {
          button.classList.add('card__info__button');
          button.textContent = "buy now"
        }
        button.addEventListener('click', () => addToCart(book.id))
        cartInfo.appendChild(button);
      }

      bk.appendChild(cartInfo);
      bookList.appendChild(bk);
    }
  }
}