import BookLoader from '../api/api';
import { bookListRender } from './books';


export function initButton() {
  const bookList: HTMLElement = <HTMLElement>document.querySelector(".lazy_button_section");
  const btn: HTMLButtonElement = document.createElement("button");
  btn.textContent = "Load more";
  btn.classList.add('lazy_button');
  btn.addEventListener("click",() => addBooks());
  bookList.appendChild(btn);
}

function addBooks() {
  const bookList: HTMLElement = <HTMLElement>document.querySelector(".main_books__goods");
  const books: NodeListOf<Element> = bookList.querySelectorAll(".book_card");
  const categories: HTMLUListElement | null = document.querySelector(".books__menu__list");
  let initialCategory: string
  if (categories) {
    const currentActive: HTMLLIElement | null = categories.querySelector(".active");
    if (currentActive) {
      initialCategory = currentActive.dataset.query ? currentActive.dataset.query : ""
      const loader = new BookLoader(initialCategory, books.length, 6);
      loader.setParams(initialCategory, books.length, 6);
      loader.getBooks().then((data) => {
        bookListRender(data, bookList)
      });
    }
  }
}