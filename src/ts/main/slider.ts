import banner1 from '../../public/images/banner1.svg';
import banner2 from '../../public/images/banner2.svg';
import banner3 from '../../public/images/banner3.svg';


const images = [
  {
    src: banner1,
  },
  {
    src: banner2,
  },
  {
    src: banner3,
  },
]



export function initSlider() {

  const sliderImages = document.querySelector(".main_slider__pictures");
  const sliderDots = document.querySelector(".main_slider__dots");

  initImages();
  initDots();
  
  function initImages(): void {
    images.forEach((image, index) => {
      const banner: HTMLImageElement = document.createElement("img");
      banner.src = image.src;
      banner.alt = 'banner' + index;
      banner.dataset.index = index.toString()
      banner.classList.add('slider__picture');
      index === 0 ? banner.classList.add('active') : null;
      if (sliderImages) {
        sliderImages.appendChild(banner);
      }
    });
  }

  function initDots(): void {
    if (sliderDots) {
      images.forEach((image, index) => {
        const point: HTMLSpanElement = document.createElement("span");
        point.dataset.index = index.toString()
        point.classList.add('slider__dot');
        index === 0 ? point.classList.add('active') : null;
        if (sliderDots) {
          sliderDots.appendChild(point); 
        }
        point.addEventListener("click",() => moveSlider(index));
      });
    }
  }
}

function moveSlider(num: number) {
  const Images: NodeListOf<HTMLImageElement> = document.querySelectorAll(".slider__picture")
  const Dots: NodeListOf<HTMLSpanElement> = document.querySelectorAll(".slider__dot")
  // Move images
  Images.forEach((pic) => {
    if (pic.classList.contains("active")) {
      pic.classList.remove("active")
    }
    if (pic.dataset.index == num.toString()) {
      pic.classList.add("active")
    }
  })
  // Move dots
  Dots.forEach((dot) => {
    if (dot.classList.contains("active")) {
      dot.classList.remove("active")
    }
    if (dot.dataset.index == num.toString()) {
      dot.classList.add("active")
    }
  })
}

export function sliderMove( ) {
  const Images: NodeListOf<HTMLImageElement> = document.querySelectorAll(".slider__picture")
  const Dots: NodeListOf<HTMLSpanElement> = document.querySelectorAll(".slider__dot")
  let current: number
  let next: number
  // Move images
  Images.forEach((pic) => {
    if (pic.classList.contains("active")) {
      if (pic.dataset.index) {
        current = +pic.dataset.index;
        pic.classList.remove("active")
      }
    }
    if (current === images.length-1) {
      next = 0
    } else {
      next = current + 1
    }
  })
  Images.forEach((pic) => {
    if (pic.dataset.index == next.toString()) {
      pic.classList.add("active");
    }
  })
  // Move dots
  Dots.forEach((dot) => {
    if (dot.classList.contains("active")) {
      if (dot.dataset.index) {
        current = +dot.dataset.index;
        dot.classList.remove("active")
      }
    }
    if (current === images.length-1) {
      next = 0
    } else {
      next = current + 1
    }
  })
  Dots.forEach((dot) => {
    if (dot.dataset.index == next.toString()) {
      dot.classList.add("active");
    }
  })
}