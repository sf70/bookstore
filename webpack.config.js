// const path = require('path');
// const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const CssMinimizerPlugin = require('css-minimizer-webpack-plugin');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const ESLintPlugin = require('eslint-webpack-plugin');
const StylelintPlugin = require('stylelint-webpack-plugin');

// module.exports = {
//   mode: 'development',
//   // entry: path.resolve(__dirname, 'src/js/index.js'),
//   entry: path.resolve(__dirname, 'src/ts/index.ts'),
//   output: {
//     filename: 'main.js',
//   },
//   resolve: {
//     extensions: ['.tsx', '.ts', '.js'],
//   },
//   devServer: {
//     open: true,
//     // Следит за этими файлами и перегружает в страницу в браузере при их изменении
//     watchFiles: ['src/**/*.html', 'src/**/**/*.scss', 'src/**/*.ts'],
//     client: {
//       logging: 'info',
//       reconnect: true,
//     },
//     static: {
//       directory: 'dist',
//     },
//     port: 3000,
//     hot: true,
//   },
//   devtool: 'inline-source-map',
//   plugins: [
//     new MiniCssExtractPlugin({
//       // filename: 'main.css',
//     }),
//     new HtmlWebpackPlugin({
//       filename: 'index.html',
//       template: './src/index.html',
//     }),
//     new ESLintPlugin({
//       extensions: ['ts'],
//       exclude: ['node_modules', 'dist', './webpack.config.js'],
//       fix: true,
//     }),
//     new StylelintPlugin({
//       fix: true,
//       extensions: ['scss'],
//       exclude: ['node_modules', 'dist'],
//     }),
//   ],
//   module: {
//     rules: [
//       {
//         test: /\.ts?$/,
//         use: 'ts-loader',
//         exclude: /node_modules/,
//       },
//       {
//         test: /\.scss$/i,
//         use: [MiniCssExtractPlugin.loader, 'css-loader', 'sass-loader'],
//       },
//       {
//         test: /\.svg$/,
//         loader: 'svg-inline-loader',
//       },
//       // {
//       //   test: /\.(png|jpg|gif|svg)$/i,
//       //   use: [
//       //     {
//       //       loader: 'url-loader',
//       //       options: {
//       //         limit: 8192,
//       //       },
//       //     },
//       //   ],
//       // },
//       // {
//       //   test: /\.html$/i,
//       //   use: 'html-loader'
//       // },
//       // {
//       //     test: /\.(png|jpg)$/i,
//       //     type: 'asset/resource',
//       //     generator: {
//       //         filename: 'public/images/name.[ext]'
//       //     }
//       // },
//       {
//         test: /\.(png|jpe?g|gif|jp2|webp|svg)$/,
//         loader: 'file-loader',
//         options: {
//           name: 'public/images/[name].[ext]',
//         },
//       },
//       {
//         test: /\.(png|jpg)$/,
//         loader: 'url-loader'
//       },
//       // {
//       //   test: /\.(png|jpe?g|gif|svg)$/i,
//       //   loader: 'file-loader',
//       //   options: {
//       //     name: 'public/images/[name].[ext]',
//       //   },
//       //   type: 'javascript/auto',
//       // },
//       // {
//       //   test: /\.(svg)$/i,
//       //   use: [
//       //     {
//       //       loader: 'url-loader',
//       //       options: {
//       //         limit: 8192,
//       //       },
//       //     },
//       //   ],
//       // },
//       // {
//       //   test: /\.pug$/i,
//       //   use: 'pug-loader',
//       // },
//     ],
//   },
//   optimization: {
//     minimizer: [
//       '...',
//       new CssMinimizerPlugin(),
//     ],
//   },
// };



const MiniCssExtractPlugin = require("mini-css-extract-plugin");

var path = require("path");

module.exports = {
  entry: path.resolve(__dirname, "src/ts/index.ts"),
  mode: "production",
  performance: {
    hints: false,
    maxEntrypointSize: 512000,
    maxAssetSize: 512000
  },
  output: {
    path: path.resolve(__dirname, "dist"),
    filename: "main.js",

    assetModuleFilename: 'public/images/[name][ext]'


  },

  devServer: {
    open: true,
    port: 3000,
    hot: true,




    watchFiles: ['src/**/*.html', 'src/**/**/*.scss', 'src/**/*.ts'],
    client: {
      logging: 'info',
      reconnect: true,
    },
    static: {
        directory: 'dist',
      },
  },
  resolve: {
    extensions: [".tsx", ".ts", ".js"],
  },
  plugins: [
    new MiniCssExtractPlugin(),
    new HtmlWebpackPlugin({
      filename: 'index.html',
      template: './src/index.html',
    }),
    new ESLintPlugin({
      extensions: ['ts'],
      exclude: ['node_modules', 'dist', './webpack.config.js'],
      fix: true,
    }),
    new StylelintPlugin({
      fix: true,
      extensions: ['scss'],
      exclude: ['node_modules', 'dist'],
    }),
  ],
  module: {
    rules: [
      {
        test: /\.s[ac]ss$/i,
        use: [MiniCssExtractPlugin.loader, "css-loader", "sass-loader"],
      },
      {
        test: /\.([cm]?ts|tsx)$/,
        loader: "ts-loader",
        exclude: /node_modules/,
      },

      {
        test: /\.(png|svg|jpg|jpeg|gif)$/i,
        type: 'asset/resource',
      },
    ],
  },
};
